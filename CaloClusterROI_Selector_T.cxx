/********************************************************************

NAME:     CaloClusterROI_Selector.cxx
PACKAGE:  offline/Reconstruction/egamma/egammaRec

AUTHORS:  A.Morley,C.A
CREATED:  Nov 2011

PURPOSE:   For each cluster create a new CaloClusterROI object and fills it then in the CaloClusterROI_Collection.
   

********************************************************************/

// INCLUDE HEADER FILES:

#include "InDetCaloClusterROISelector/CaloClusterROI_Selector.h"

#include "InDetRecToolInterfaces/ICaloClusterROI_Builder.h"
//
#include "CaloEvent/CaloCellContainer.h"
#include "CaloUtils/CaloCellList.h"

#include "xAODCaloEvent/CaloClusterContainer.h"


//Cluster cuts
#include "egammaInterfaces/IegammaCheckEnergyDepositTool.h"
#include "egammaInterfaces/IegammaIso.h"
#include "egammaInterfaces/IegammaMiddleShape.h"
#include "TrkCaloClusterROI/CaloClusterROI.h"
#include "TrkCaloClusterROI/CaloClusterROI_Collection.h"



// INCLUDE GAUDI HEADER FILES:
#include "GaudiKernel/MsgStream.h"

#include <algorithm> 
#include <math.h>

//  END OF HEADER FILES INCLUDE

/////////////////////////////////////////////////////////////////

//  CONSTRUCTOR:
    
InDet::CaloClusterROI_Selector::CaloClusterROI_Selector(const std::string& name, 
             ISvcLocator* pSvcLocator): 
  AthAlgorithm(name, pSvcLocator),
  m_EMClusters(0),
  m_TopoClusters(0),
  m_SelectedEMClusters(0),
  m_SelectedTopoClusters(0),
  m_timingProfile(0)
{
  // The following properties are specified at run-time
  // (declared in jobOptions file)
  // input cluster for egamma objects
  declareProperty("InputClusterContainerName",
                  m_inputClusterContainerName="LArClusterEM",
                  "Input cluster for egamma objects");
  
  //Cell container
  declareProperty("CellsName",                              
                  m_cellsName="AllCalo",      
                  "Names of containers which contain cells ");

  // input cluster for egamma objects
  declareProperty("OutputClusterContainerName",
                  m_outputClusterContainerName="CaloClusterROIs",
                  // in script: InDetCaloClusterROIs
                  "Output cluster for egamma objects");
  //
  // Handles of tools
  //Hadronic Isolation
  declareProperty("EMCaloIsolationTool",                    m_emCaloIsolationTool, "Handle of the EMCaloIsolationTool");
  //Check Fraction 
  declareProperty("egammaCheckEnergyDepositTool",           m_egammaCheckEnergyDepositTool, "Handle of the egammaCheckEnergyDepositTool");
  //
  declareProperty("CaloClusterROIBuilder",                  m_caloClusterROI_Builder,"Handle of the CaloClusterROI_Builder Tool");
  //
  // Other properties.
  //
  declareProperty("CheckEMSamples",                         m_CheckEMsamples =true);
  declareProperty("CheckHadronicEnergy",                    m_CheckHadronicEnergy=true);
  declareProperty("CheckReta",                              m_CheckReta=true);
  //
  declareProperty("HadRatioCut",                            m_HadRatioCut  =0.12,  " Cut on Hadronic Leakage");
  declareProperty("RetaCut",                                m_RetaCut      =0.65,  " Cut on Reta");
  declareProperty("ClusterEtCut",                           m_ClusterEtCut =1000,  " Cut On Cluster Et");
  

}

// ================================================================
InDet::CaloClusterROI_Selector::~CaloClusterROI_Selector()
{  
  //
  // destructor
  //
}

// =================================================================
StatusCode InDet::CaloClusterROI_Selector::initialize()
{
  //
  // initialize method
  //

  ATH_MSG_DEBUG("Initializing CaloClusterROI_Selector");

  /*Get the check Energy Deposit tool*/
  if(m_egammaCheckEnergyDepositTool.retrieve().isFailure()) {
    ATH_MSG_ERROR("Unable to retrieve "<<m_egammaCheckEnergyDepositTool);
    return StatusCode::FAILURE;
  }

  /*Get the Hadronic iso tool*/
  if(m_CheckHadronicEnergy){
    if(m_emCaloIsolationTool.retrieve().isFailure()) {
      ATH_MSG_ERROR("Unable to retrieve "<<m_emCaloIsolationTool);
      return StatusCode::FAILURE;
    }
  }
  /* Get the middle shapes Tool*/
  if(m_CheckReta){
    // Create egammaMiddleShape Tool
    std::string egammaMiddleShapeTool_name="egammaMiddleShape/Roiegammamiddleshape";
    m_egammaMiddleShape=ToolHandle<IegammaMiddleShape>(egammaMiddleShapeTool_name);
    // a priori this is not useful
    if(m_egammaMiddleShape.retrieve().isFailure()) {
      ATH_MSG_WARNING("Unable to retrieve "<<m_egammaMiddleShape);
      return StatusCode::SUCCESS;
    } 
    else ATH_MSG_DEBUG("Tool " << m_egammaMiddleShape << " retrieved");  
  }
  
  if(m_caloClusterROI_Builder.retrieve().isFailure()) {
    ATH_MSG_ERROR("Unable to retrieve "<< m_caloClusterROI_Builder);
    return StatusCode::FAILURE;
  } 
  else ATH_MSG_DEBUG("Retrieved Tool "<< m_caloClusterROI_Builder); 


  m_timingProfile = 0;
  StatusCode sc = service("ChronoStatSvc",m_timingProfile);
  if(sc.isFailure() || m_timingProfile == 0) {
    ATH_MSG_ERROR("Cannot find the ChronoStatSvc " << m_timingProfile);
  }

  m_EMClusters=0;
  m_TopoClusters=0;
  m_SelectedEMClusters=0;
  m_SelectedTopoClusters=0;

  ATH_MSG_INFO("Initialization completed successfully");
  return StatusCode::SUCCESS;
}



// ====================================================================
StatusCode InDet::CaloClusterROI_Selector::finalize()
{
  //
  // finalize method
  //
  ATH_MSG_INFO ("EMClusters "       << m_EMClusters);
  ATH_MSG_INFO ("TopoClusters "     << m_TopoClusters);
  // ATH_MSG_INFO ("EMClusters_eta() " << m_SelectedEMClusters);
  // for (std::vector<double>::const_iterator it = EMetas.begin(); it < EMetas.end(); ++it) 
	// 	std::cout << *it << std::endl;
  // ATH_MSG_INFO ("EMClusters_etaBE(2) " << m_SelectedEMClusters);
  // for (std::vector<double>::const_iterator it = EMetas_BE.begin(); it < EMetas_BE.end(); ++it) 
	// 	std::cout << *it << std::endl;
  // ATH_MSG_INFO ("EMClusters_e237() " << m_SelectedEMClusters);
  // for (std::vector<double>::const_iterator it = EM_e237.begin(); it < EM_e237.end(); ++it) 
	// 	std::cout << *it << std::endl;
  // ATH_MSG_INFO ("EMClusters_e277() " << m_SelectedEMClusters);
  // for (std::vector<double>::const_iterator it = EM_e277.begin(); it < EM_e277.end(); ++it) 
	// 	std::cout << *it << std::endl;
  // ATH_MSG_INFO ("EMClusters_ethad() " << m_SelectedEMClusters);
  // for (std::vector<double>::const_iterator it = EM_ethad.begin(); it < EM_ethad.end(); ++it) 
	// 	std::cout << *it << std::endl;
  // ATH_MSG_INFO ("EMClusters_et() " << m_SelectedEMClusters);
  // for (std::vector<double>::const_iterator it = EM_et.begin(); it < EM_et.end(); ++it) 
	// 	std::cout << *it << std::endl;

  ATH_MSG_INFO ("TopoClusters_eta() " << m_SelectedTopoClusters);
  for (std::vector<double>::const_iterator it = Topoetas.begin(); it < Topoetas.end(); ++it) 
		std::cout << *it << std::endl;
  ATH_MSG_INFO ("TopoClusters_etaBE(2) " << m_SelectedTopoClusters);
  for (std::vector<double>::const_iterator it = Topoetas_BE.begin(); it < Topoetas_BE.end(); ++it) 
		std::cout << *it << std::endl;
  ATH_MSG_INFO ("TopoClusters_e237() " << m_SelectedTopoClusters);
  for (std::vector<double>::const_iterator it = Topo_e237.begin(); it < Topo_e237.end(); ++it) 
		std::cout << *it << std::endl;
  ATH_MSG_INFO ("TopoClusters_e277() " << m_SelectedTopoClusters);
  for (std::vector<double>::const_iterator it = Topo_e277.begin(); it < Topo_e277.end(); ++it) 
		std::cout << *it << std::endl;
  ATH_MSG_INFO ("TopoClusters_ethad() " << m_SelectedTopoClusters);
  for (std::vector<double>::const_iterator it = Topo_ethad.begin(); it < Topo_ethad.end(); ++it) 
		std::cout << *it << std::endl;
  ATH_MSG_INFO ("TopoClusters_et() " << m_SelectedTopoClusters);
  for (std::vector<double>::const_iterator it = Topo_et.begin(); it < Topo_et.end(); ++it) 
		std::cout << *it << std::endl;

  ATH_MSG_INFO ("ROISize " << ROIsize.size() );  
  for(unsigned int i = 0; i < ROIsize.size() ; ++i)
		std::cout << ROIsize[i] << std::endl;


  return StatusCode::SUCCESS;
}

// ======================================================================
StatusCode InDet::CaloClusterROI_Selector::execute()
{
  //
  // athena execute method
  //
  
//LAr+Topo;

  //bool do_trackMatch = true;
  ATH_MSG_DEBUG("Executing CaloClusterROI_Selector");

  StatusCode sc;
  // Chrono name for each Tool
  std::string chronoName;

  // Record output CaloClusterROICollection:
  CaloClusterROI_Collection* ccROI_Collection =  new CaloClusterROI_Collection();
  
  sc = evtStore()->record( ccROI_Collection, m_outputClusterContainerName );
  if (sc.isFailure()) 
  {
    ATH_MSG_ERROR("Could not record "<< m_outputClusterContainerName <<" object.");
    return StatusCode::FAILURE;
  }
    
  // retrieve cluster containers, return `failure' if not existing
  const xAOD::CaloClusterContainer* inputClusterContainer;
  if(  evtStore()->contains<xAOD::CaloClusterContainer>(m_inputClusterContainerName)) {  
    sc = evtStore()->retrieve(inputClusterContainer,  m_inputClusterContainerName);
    if( sc.isFailure() ) {
      ATH_MSG_ERROR("Input Cluster not retrived but found " << m_inputClusterContainerName);
      ATH_MSG_DEBUG("Locking ROI container  and returning");
      evtStore()->setConst(ccROI_Collection).ignore();
      return StatusCode::SUCCESS;
    }
  } else { 
    ATH_MSG_INFO("No input Cluster container found " << m_inputClusterContainerName);
    ATH_MSG_DEBUG("Locking ROI container  and returning");
    evtStore()->setConst(ccROI_Collection).ignore();
    return StatusCode::SUCCESS;
  }
  
  
  // retrieve Calo Cell Container
  const CaloCellContainer* m_cellcoll(0);
  if(m_CheckHadronicEnergy|| m_CheckReta){
    if(  evtStore()->contains<CaloCellContainer>(m_cellsName) ){  
      StatusCode sc = evtStore()->retrieve(m_cellcoll, m_cellsName) ; 
      if(sc.isFailure() || !m_cellcoll) {
        ATH_MSG_WARNING("no Calo Cell Container " << m_cellsName << " found");
        return sc;
      } 
    } else {
      ATH_MSG_DEBUG("No input Cell container found " << m_cellsName << ". Will not apply shape cuts");
      ATH_MSG_DEBUG("Locking ROI container  and returning");
      //    evtStore()->setConst(ccROI_Collection).ignore();
      //    return StatusCode::SUCCESS;
      m_CheckHadronicEnergy =  false;
      m_CheckReta = false;
      
    }
  }

  // loop over clusters in the default inputClusterContainer:
  typedef xAOD::CaloClusterContainer::const_iterator clus_iterator;
  // loop over clusters 
  // for(clus_iterator iter = inputClusterContainer->begin();
  //                   iter != inputClusterContainer->end(); 
  //                   ++iter) 
  // {
  //   m_EMClusters++;
  //   const xAOD::CaloCluster* cluster = *iter;    
  //   if (PassClusterSelection(cluster , m_cellcoll, 1))
  //   {
  //     m_SelectedEMClusters++;
  //     ATH_MSG_DEBUG("Pass EM cluster selection");
  //     Trk::CaloClusterROI* ccROI = m_caloClusterROI_Builder->buildClusterROI( cluster );  
  //     ccROI_Collection->push_back(ccROI);   
  //   } else {
  //    ATH_MSG_DEBUG("Fail cluster selection");
  //   }
  //   // reset sc to success for each cluster
  //   sc = StatusCode::SUCCESS;
  // } // end loop over all Calorimeter clusters 

  // // May be left unchecked if we exited the loop via a continue.
  // sc.ignore();

 
  //After LAr run let's begin with Topo Clusters in Forward region.
  const xAOD::CaloClusterContainer* inputTopoClusterContainer;
  if(  evtStore()->contains<xAOD::CaloClusterContainer>("CaloCalTopoClusters")) {  
    sc = evtStore()->retrieve(inputTopoClusterContainer,  "CaloCalTopoClusters");
    if( sc.isFailure() ) {
      ATH_MSG_ERROR("Input Cluster not retrived but found CaloCalTopoClusters");
      ATH_MSG_DEBUG("Locking ROI container  and returning");
      evtStore()->setConst(ccROI_Collection).ignore();
      return StatusCode::SUCCESS;
    }
  } else { 
    ATH_MSG_INFO("No input Cluster container found CaloCalTopoClusters");
    ATH_MSG_DEBUG("Locking ROI container  and returning");
    evtStore()->setConst(ccROI_Collection).ignore();
    return StatusCode::SUCCESS;
  }

for(clus_iterator iter  = inputTopoClusterContainer->begin();
                  iter != inputTopoClusterContainer->end(); 
                    ++iter) 
  {
    m_TopoClusters++;
    const xAOD::CaloCluster* cluster = *iter;    
    if (PassClusterSelection(cluster , m_cellcoll, 2))
    {
      m_SelectedTopoClusters++;
      ATH_MSG_DEBUG("Pass Topo cluster selection");
      Trk::CaloClusterROI* ccROI = m_caloClusterROI_Builder->buildClusterROI( cluster );  
      ccROI_Collection->push_back(ccROI);   
    } else {
     ATH_MSG_DEBUG("Fail cluster selection");
    }
    // reset sc to success for each cluster
    sc = StatusCode::SUCCESS;
  } // end loop over all Calorimeter clusters 

  // May be left unchecked if we exited the loop via a continue.
  sc.ignore();



  // lock the egamma collection
  evtStore()->setConst(ccROI_Collection).ignore();
  ATH_MSG_DEBUG("execute completed successfully");
  // ATH_MSG_INFO ("SelectedClusters size " << ccROI_Collection->size());
  // std::cout << "Kaili_DEBUG ROI size " <<":" << ccROI_Collection->size() << std::endl;
  ROIsize.push_back( ccROI_Collection->size() );
  return StatusCode::SUCCESS;
}
// ======================================================================
bool InDet::CaloClusterROI_Selector::PassClusterSelection(const xAOD::CaloCluster* cluster ,  const CaloCellContainer* cellcoll, const int type)
{
 
  if( m_CheckEMsamples && !m_egammaCheckEnergyDepositTool->checkFractioninSamplingCluster( cluster ) ) {
    ATH_MSG_DEBUG("Cluster failed sample check: dont make ROI");
    return false;
  }
  
  if((m_CheckHadronicEnergy || m_CheckReta) && cellcoll==0) {
    ATH_MSG_DEBUG("No cell collection: dont make ROI");
    return false;
  }
    
 
  // transverse energy in calorimeter (using eta position in second sampling)
  double eta2   = fabs(cluster->etaBE(2));  
  if (type==1 && (eta2>2.47 || eta2<0.)) 
  {
    ATH_MSG_DEBUG("EM Cluster etaBE(2) failed: dont make ROI");
    return false;
  }

  double eta=eta2;
  if (type==1)  eta = eta2;
  if (type==2)  eta = fabs(cluster->eta()); 

  if (type==2) 
    {
      if ( eta<0.    )   return false;
      // if ( eta<=2.47 )   return false;  // For Topo Clusters only foraward region used;
      // if ( eta>=4.8  )   return false;  // 2.47-4.8 Max at 4.8 
    }


  double et = cosh(eta)!=0. ? cluster->e()/cosh(eta) : 0.;
  // std::cout << "Kaili_DEBUG Pass, Cluster Et " <<":" << et << std::endl;

  if ( et < m_ClusterEtCut )
  {
    ATH_MSG_DEBUG("Cluster failed Energy Cut: dont make ROI");
    return false;
  }
    
double ethad1 = 0;
double ethad  = 0;
double e237    =0;
double e277    =0;

  if(m_CheckReta || m_CheckHadronicEnergy){

    if(m_CheckReta){
      StatusCode sc = m_egammaMiddleShape->execute(cluster,cellcoll);
      if ( sc.isFailure() ) {
        ATH_MSG_WARNING("call to Middle shape returns failure for execute");
        return false;
      }
        e237   = m_egammaMiddleShape->e237(); //brief uncalibrated energy (sum of cells) of the middle sampling in a rectangle of size 3x7
      // E(7*7) in 2nd sampling
        e277   = m_egammaMiddleShape->e277(); //brief uncalibrated energy (sum of cells) of the middle sampling in a rectangle of size 7x7
      if (e277 != 0. && e237/e277<=m_RetaCut){
        ATH_MSG_DEBUG("Cluster failed Reta test: dont make ROI");
        return false;
      }
    }
    if(m_CheckHadronicEnergy){
      // define a new Calo Cell list corresponding to HAD Calo
      CaloCell_ID::SUBCALO theCalo1 =  static_cast<CaloCell_ID::SUBCALO>(CaloCell_ID::LARHEC) ;
      CaloCell_ID::SUBCALO theCalo2 =  static_cast<CaloCell_ID::SUBCALO>(CaloCell_ID::TILE) ;
      std::vector<CaloCell_ID::SUBCALO> theVecCalo;
      theVecCalo.push_back(theCalo1);
      theVecCalo.push_back(theCalo2);
      // define a new Calo Cell list
      CaloCellList* HADccl = new CaloCellList(cellcoll,theVecCalo); 
      StatusCode sc = m_emCaloIsolationTool->execute(cluster,HADccl); 
      if ( sc.isFailure() ) {
        ATH_MSG_WARNING("call to Iso returns failure for execute");
        delete HADccl;
        return false;
      }
      delete HADccl;
      //brief transverse energy in the first sampling of the hadronic calorimeters behind the cluster  
      ethad1 = m_emCaloIsolationTool->ethad1(); //CaloSampling::HEC0 + CaloSampling::TileBar0 + CaloSampling::TileExt0 - CaloSampling::TileGap3   
      ethad  = m_emCaloIsolationTool->ethad();  //CaloSampling::HEC0 + CaloSampling::TileBar0 + CaloSampling::TileExt0
      double raphad1 = fabs(et) > 0. ? ethad1/et : 0.; 
      double raphad  = fabs(et) > 0. ? ethad/et : 0.;
      if (eta >= 0.8 && eta < 1.37){
        if (raphad >m_HadRatioCut){
          ATH_MSG_DEBUG("Cluster failed Hadronic Leakage test: dont make ROI");
          return false;
        }
			}
      else if(raphad1 >m_HadRatioCut){
        ATH_MSG_DEBUG("Cluster failed Hadronic Leakage test: dont make ROI");
        return false;
      }
    }
  }

// 0.8-1.37 ethad
// 0-0.8, 1.37~2.56 ethad1

if (eta<0.8 || eta>=1.37)
{
  ethad = ethad1;
}

if (type==1)
{
      EMetas.push_back(    cluster->eta() );
      EMetas_BE.push_back( cluster->etaBE(2) );
      EM_e237.push_back(    e237  );
      EM_e277.push_back(    e277  );
      EM_ethad.push_back(   ethad );
      EM_et.push_back(   et );
}
    // if (type==2)  //central
    // {
    //       Topoetas.push_back(    cluster->eta() );
    //       Topoetas_BE.push_back( cluster->etaBE(2) );
    //       Topo_e237.push_back(    e237  );
    //       Topo_e277.push_back(    e277  );
    //       Topo_ethad.push_back(   ethad );
    //       Topo_et.push_back(   et );
    // }
if (type==2) //forward
{
      Topoetas.push_back(    cluster->eta() );
      Topoetas_BE.push_back( cluster->etaBE(2) );
      Topo_e237.push_back(    e237  );
      Topo_e277.push_back(    e277  );
      Topo_ethad.push_back(   ethad );
      Topo_et.push_back(   et );
}
  return true;
}
