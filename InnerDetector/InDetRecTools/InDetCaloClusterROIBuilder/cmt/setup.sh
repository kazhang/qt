# echo "setup InDetCaloClusterROIBuilder InDetCaloClusterROIBuilder-r737712 in /afs/cern.ch/user/k/kazhang/QT/qt/InnerDetector/InDetRecTools"

if test "${CMTROOT}" = ""; then
  CMTROOT=/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.12/CMT/v1r25p20160527; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
cmtInDetCaloClusterROIBuildertempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if test ! $? = 0 ; then cmtInDetCaloClusterROIBuildertempfile=/tmp/cmt.$$; fi
${CMTROOT}/${CMTBIN}/cmt.exe setup -sh -pack=InDetCaloClusterROIBuilder -version=InDetCaloClusterROIBuilder-r737712 -path=/afs/cern.ch/user/k/kazhang/QT/qt/InnerDetector/InDetRecTools  -quiet -without_version_directory -no_cleanup $* >${cmtInDetCaloClusterROIBuildertempfile}
if test $? != 0 ; then
  echo >&2 "${CMTROOT}/${CMTBIN}/cmt.exe setup -sh -pack=InDetCaloClusterROIBuilder -version=InDetCaloClusterROIBuilder-r737712 -path=/afs/cern.ch/user/k/kazhang/QT/qt/InnerDetector/InDetRecTools  -quiet -without_version_directory -no_cleanup $* >${cmtInDetCaloClusterROIBuildertempfile}"
  cmtsetupstatus=2
  /bin/rm -f ${cmtInDetCaloClusterROIBuildertempfile}
  unset cmtInDetCaloClusterROIBuildertempfile
  return $cmtsetupstatus
fi
cmtsetupstatus=0
. ${cmtInDetCaloClusterROIBuildertempfile}
if test $? != 0 ; then
  cmtsetupstatus=2
fi
/bin/rm -f ${cmtInDetCaloClusterROIBuildertempfile}
unset cmtInDetCaloClusterROIBuildertempfile
return $cmtsetupstatus

