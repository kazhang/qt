# echo "setup InDetCaloClusterROIBuilder InDetCaloClusterROIBuilder-r737712 in /afs/cern.ch/user/k/kazhang/QT/qt/InnerDetector/InDetRecTools"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.12/CMT/v1r25p20160527
endif
source ${CMTROOT}/mgr/setup.csh
set cmtInDetCaloClusterROIBuildertempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if $status != 0 then
  set cmtInDetCaloClusterROIBuildertempfile=/tmp/cmt.$$
endif
${CMTROOT}/${CMTBIN}/cmt.exe setup -csh -pack=InDetCaloClusterROIBuilder -version=InDetCaloClusterROIBuilder-r737712 -path=/afs/cern.ch/user/k/kazhang/QT/qt/InnerDetector/InDetRecTools  -quiet -without_version_directory -no_cleanup $* >${cmtInDetCaloClusterROIBuildertempfile}
if ( $status != 0 ) then
  echo "${CMTROOT}/${CMTBIN}/cmt.exe setup -csh -pack=InDetCaloClusterROIBuilder -version=InDetCaloClusterROIBuilder-r737712 -path=/afs/cern.ch/user/k/kazhang/QT/qt/InnerDetector/InDetRecTools  -quiet -without_version_directory -no_cleanup $* >${cmtInDetCaloClusterROIBuildertempfile}"
  set cmtsetupstatus=2
  /bin/rm -f ${cmtInDetCaloClusterROIBuildertempfile}
  unset cmtInDetCaloClusterROIBuildertempfile
  exit $cmtsetupstatus
endif
set cmtsetupstatus=0
source ${cmtInDetCaloClusterROIBuildertempfile}
if ( $status != 0 ) then
  set cmtsetupstatus=2
endif
/bin/rm -f ${cmtInDetCaloClusterROIBuildertempfile}
unset cmtInDetCaloClusterROIBuildertempfile
exit $cmtsetupstatus

