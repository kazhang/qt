# echo "cleanup InDetCaloClusterROISelector InDetCaloClusterROISelector-r737776 in /afs/cern.ch/user/k/kazhang/QT/qt/InnerDetector/InDetRecAlgs"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.12/CMT/v1r25p20160527
endif
source ${CMTROOT}/mgr/setup.csh
set cmtInDetCaloClusterROISelectortempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if $status != 0 then
  set cmtInDetCaloClusterROISelectortempfile=/tmp/cmt.$$
endif
${CMTROOT}/${CMTBIN}/cmt.exe cleanup -csh -pack=InDetCaloClusterROISelector -version=InDetCaloClusterROISelector-r737776 -path=/afs/cern.ch/user/k/kazhang/QT/qt/InnerDetector/InDetRecAlgs  -quiet -without_version_directory $* >${cmtInDetCaloClusterROISelectortempfile}
if ( $status != 0 ) then
  echo "${CMTROOT}/${CMTBIN}/cmt.exe cleanup -csh -pack=InDetCaloClusterROISelector -version=InDetCaloClusterROISelector-r737776 -path=/afs/cern.ch/user/k/kazhang/QT/qt/InnerDetector/InDetRecAlgs  -quiet -without_version_directory $* >${cmtInDetCaloClusterROISelectortempfile}"
  set cmtcleanupstatus=2
  /bin/rm -f ${cmtInDetCaloClusterROISelectortempfile}
  unset cmtInDetCaloClusterROISelectortempfile
  exit $cmtcleanupstatus
endif
set cmtcleanupstatus=0
source ${cmtInDetCaloClusterROISelectortempfile}
if ( $status != 0 ) then
  set cmtcleanupstatus=2
endif
/bin/rm -f ${cmtInDetCaloClusterROISelectortempfile}
unset cmtInDetCaloClusterROISelectortempfile
exit $cmtcleanupstatus

