# echo "cleanup InDetCaloClusterROISelector InDetCaloClusterROISelector-r737776 in /afs/cern.ch/user/k/kazhang/QT/qt/InnerDetector/InDetRecAlgs"

if test "${CMTROOT}" = ""; then
  CMTROOT=/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.12/CMT/v1r25p20160527; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
cmtInDetCaloClusterROISelectortempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if test ! $? = 0 ; then cmtInDetCaloClusterROISelectortempfile=/tmp/cmt.$$; fi
${CMTROOT}/${CMTBIN}/cmt.exe cleanup -sh -pack=InDetCaloClusterROISelector -version=InDetCaloClusterROISelector-r737776 -path=/afs/cern.ch/user/k/kazhang/QT/qt/InnerDetector/InDetRecAlgs  -quiet -without_version_directory $* >${cmtInDetCaloClusterROISelectortempfile}
if test $? != 0 ; then
  echo >&2 "${CMTROOT}/${CMTBIN}/cmt.exe cleanup -sh -pack=InDetCaloClusterROISelector -version=InDetCaloClusterROISelector-r737776 -path=/afs/cern.ch/user/k/kazhang/QT/qt/InnerDetector/InDetRecAlgs  -quiet -without_version_directory $* >${cmtInDetCaloClusterROISelectortempfile}"
  cmtcleanupstatus=2
  /bin/rm -f ${cmtInDetCaloClusterROISelectortempfile}
  unset cmtInDetCaloClusterROISelectortempfile
  return $cmtcleanupstatus
fi
cmtcleanupstatus=0
. ${cmtInDetCaloClusterROISelectortempfile}
if test $? != 0 ; then
  cmtcleanupstatus=2
fi
/bin/rm -f ${cmtInDetCaloClusterROISelectortempfile}
unset cmtInDetCaloClusterROISelectortempfile
return $cmtcleanupstatus

