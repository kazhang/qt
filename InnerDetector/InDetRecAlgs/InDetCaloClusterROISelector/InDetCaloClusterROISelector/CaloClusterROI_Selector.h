#ifndef EGAMMAREC_CALOCLUSTERROI_SELECTOR_H
#define EGAMMAREC_CALOCLUSTERROI_SELECTOR_H
/**
  @class CaloClusterROI_Selector
          Algorithm which creates an CaloClusterROICollection. 
          It retrieves data objects from TDS 
*/

// INCLUDE HEADER FILES:
#include <vector>
#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ToolHandle.h"
#include "GaudiKernel/IChronoStatSvc.h"

class IEMShowerBuilder;
class IegammaCheckEnergyDepositTool;
class IegammaIso;
class IegammaMiddleShape;


#include "xAODCaloEvent/CaloClusterFwd.h"

class CaloCellContainer;

namespace InDet {

class ICaloClusterROI_Builder;


class CaloClusterROI_Selector : public AthAlgorithm
{
 public:

  /** @brief Default constructor*/
  CaloClusterROI_Selector(const std::string& name, ISvcLocator* pSvcLocator);

  /** @brief Destructor*/
  ~CaloClusterROI_Selector();
	
  /** @brief initialize method*/
  StatusCode initialize();
  /** @brief finalize method*/
  StatusCode finalize();
  /** @brief execute method*/
  StatusCode execute();

 private:
  bool PassClusterSelection(const xAOD::CaloCluster* cluster ,  const CaloCellContainer* cellcoll, const int type) ;
  /** @brief Name of the cluster intput collection*/
  std::string  m_inputClusterContainerName;
  /** @brief Name of the cluster output collection*/
  std::string  m_outputClusterContainerName;
  /** @brief Name of the cells container*/
  std::string  m_cellsName;

  //
  // The tools
  //
  // subalgorithm pointers cached in initialize:
  /** @brief isolation tool for Ethad1 / Ethad*/
  ToolHandle<IegammaIso>                       m_emCaloIsolationTool;
  /** @brief Pointer to the egammaCheckEnergyDepositTool*/
  ToolHandle<IegammaCheckEnergyDepositTool>    m_egammaCheckEnergyDepositTool;
  /** @Middle shape Tool*/
  ToolHandle<IegammaMiddleShape>               m_egammaMiddleShape;
  /** @brief Tool to build ROI*/
  ToolHandle<InDet::ICaloClusterROI_Builder>   m_caloClusterROI_Builder;
  //
  // All booleans
  //
  bool                              m_CheckEMsamples;
  bool                              m_CheckHadronicEnergy;
  bool                              m_CheckReta;
  //
  // Other properties.
  //
  /** @brief Cut on hadronic leakage*/
  double              m_HadRatioCut;
  double              m_RetaCut;
  double              m_ClusterEtCut;
	unsigned int         m_EMClusters;
  unsigned int         m_TopoClusters;
  unsigned int         m_SelectedEMClusters;
  unsigned int         m_SelectedTopoClusters;


  std::vector<unsigned int> ROIsize;
  
  std::vector<double>       EM_e237;
  std::vector<double>       EM_e277;
  std::vector<double>       EM_et;
  std::vector<double>       EM_ethad1;
  std::vector<double>       EM_ethad;
  std::vector<double>       EMetas;
  std::vector<double>       EMetas_BE;

  std::vector<double>       Topo_e237;
  std::vector<double>       Topo_e277;
  std::vector<double>       Topo_et;
  std::vector<double>       Topo_ethad1;
  std::vector<double>       Topo_ethad;
  std::vector<double>       Topoetas;
  std::vector<double>       Topoetas_BE;


  // others:
  IChronoStatSvc* m_timingProfile;

};

} //End namespace 
#endif
