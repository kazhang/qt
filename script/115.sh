#!/bin/bash

# nb: $1 starts counting from 0, we start from 1
inde=$(echo "0+$1" | bc -l)  # 0-9
index=$(printf "%01d" "$inde")
echo $index

cd /afs/cern.ch/user/k/kazhang/eos/0115/

if [ ! -d "${index}" ]
then 
    mkdir ${index}
    cd    ${index}
else 
    cd    ${index}
fi

if [ ! -e "for_${index}.root" ] 
then
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
cd /afs/cern.ch/user/k/kazhang/QT/
asetup AtlasProduction,20.20.12.1,here
cd /afs/cern.ch/user/k/kazhang/eos/0115/${index}
rm -rf *
Reco_tf.py --inputRDOFile /afs/cern.ch/user/k/kazhang/eos/mc15_14TeV.422029.ParticleGun_single_ele_Pt10.recon.RDO.e5286_s3348_s3347_r10846/RDO.15578049._0000${index}* --outputDAOD_IDTRKVALIDFile for_${index}.root \
--maxEvents 100000 --digiSteeringConf 'StandardInTimeOnlyTruth' --conditionsTag 'default:OFLCOND-MC15c-SDR-14-04' --postInclude 'all:PyJobTransforms/UseFrontier.py,InDetSLHC_Example/postInclude.SLHC_Setup_InclBrl_4_25x100.py,InDetSLHC_Example/postInclude.SLHC_Setup.py' 'HITtoRDO:InDetSLHC_Example/postInclude.SLHC_Digitization_lowthresh.py' 'RAWtoESD:InDetSLHC_Example/postInclude.DigitalClustering.py,InDetSLHC_Example/postInclude.NoJetPtFilter.py' --preExec 'all:from AthenaCommon.GlobalFlags import globalflags; globalflags.DataSource.set_Value_and_Lock("geant4");from InDetSLHC_Example.SLHC_JobProperties import SLHC_Flags;SLHC_Flags.doGMX.set_Value_and_Lock(True);SLHC_Flags.LayoutOption="InclinedAlternative";' 'HITtoRDO:from Digitization.DigitizationFlags import digitizationFlags; digitizationFlags.doInDetNoise.set_Value_and_Lock(False); digitizationFlags.overrideMetadata+=["SimLayout","PhysicsList"];' 'ESDtoDPD:rec.DPDMakerScripts.set_Value_and_Lock(["InDetPrepRawDataToxAOD/InDetDxAOD.py","PrimaryDPDMaker/PrimaryDPDMaker.py"]);' --preInclude 'all:InDetSLHC_Example/preInclude.SLHC.py,InDetSLHC_Example/preInclude.SLHC_Setup_InclBrl_4_25x100.py,InDetSLHC_Example/preInclude.SLHC_Setup_Strip_GMX.py,InDetSLHC_Example/preInclude.SLHC_Calorimeter_mu0.py' 'HITtoRDO:InDetSLHC_Example/preInclude.SLHC.py,InDetSLHC_Example/preInclude.NoTRT_NoBCM_NoDBM.py' 'default:InDetSLHC_Example/preInclude.SLHC.NoTRT_NoBCM_NoDBM.Reco.py,InDetSLHC_Example/SLHC_Setup_Reco_TrackingGeometry_GMX.py' 'RDOMergeAthenaMP:InDetSLHC_Example/preInclude.SLHC.py,InDetSLHC_Example/preInclude.NoTRT_NoBCM_NoDBM.py,InDetSLHC_Example/SLHC_Setup_Reco_TrackingGeometry_GMX.py' 'POOLMergeAthenaMPAOD0:InDetSLHC_Example/preInclude.SLHC.NoTRT_NoBCM_NoDBM.Ana.py,InDetSLHC_Example/SLHC_Setup_Reco_Alpine.py' 'POOLMergeAthenaMPDAODIDTRKVALID0:InDetSLHC_Example/preInclude.SLHC.NoTRT_NoBCM_NoDBM.Ana.py,InDetSLHC_Example/SLHC_Setup_Reco_Alpine.py' --DataRunNumber '241700' --postExec 'all:ToolSvc.LayoutTranslationHelper.translateIdentifiersForInclinedAlternative=True;' 'HITtoRDO:pixeldigi.EnableSpecialPixels=False; CfgMgr.MessageSvc().setError+=["HepMcParticleLink"];' 'RAWtoESD:ToolSvc.InDetSCT_ClusteringTool.useRowInformation=True;' --geometryVersion 'default:ATLAS-P2-ITK-17-00-01'
else
echo "file existing. Exit gracefully"
fi
# copy your output to eg eos
# careful here to get hte augomatically created name right DAOD_TRUTH3 + your string
# xrdcp DAOD_TRUTH3.DAOD.root root://eosuser/${out_dir}/DAOD_TRUTH3_${index}.pool.root
# xrdcp out.out root://eosuser/${out_dir}/out_${index}.out

